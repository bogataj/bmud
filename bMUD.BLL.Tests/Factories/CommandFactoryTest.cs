﻿using bMUD.BLL.Commands;
using bMUD.BLL.Factories;
using Moq;
using NUnit.Framework;
using Prism.Events;

namespace bMUD.BLL.Tests.Factories
{
    public class CommandFactoryTest
    {
        private readonly ICommandFactory sut;

        Mock<IEventAggregator> eventAggregatorMock = new Mock<IEventAggregator>();

        public CommandFactoryTest()
        {
            sut = new CommandFactory(eventAggregatorMock.Object);
        }

        [Test]
        public void EmptyCommandStringReturnsNull()
        {
            var cmd = sut.CreateCommand("");

            Assert.IsNull(cmd);
        }

        [Test]
        public void NoneCommandStringReturnsNull()
        {
            var cmd = sut.CreateCommand("This is not a command");

            Assert.IsNull(cmd);
        }

        [Test]
        public void AliasStringReturnsAliasCommand()
        {
            var cmd = sut.CreateCommand("/alias");

            Assert.That(cmd, Is.TypeOf<AliasCommand>());
        }

        [Test]
        public void UnAliasStringReturnsUnAliasCommand()
        {
            var cmd = sut.CreateCommand("/unalias");

            Assert.That(cmd, Is.TypeOf<UnAliasCommand>());
        }
    }
}
