﻿using bMUD.BLL.Services;
using bMUD.Core.Alias;
using bMUD.Core.Events;
using bMUD.Core.Messages;
using bMUD.DAL.Repositories;
using Moq;
using NUnit.Framework;
using Prism.Events;
using System;

namespace bMUD.BLL.Tests.Services
{
    public class AliasServiceTest
    {
        private Mock<IAliasRepository> aliasRepositoryMock;
        private Mock<IEventAggregator> eventAggregatorMock;
        private Mock<MessageEvent> messageEventMock;
        private Mock<AliasEvent> aliasEventMock;

        private IAliasService sut;

        [SetUp]
        public void AliasServiceTestSetup()
        {
            this.aliasRepositoryMock = new Mock<IAliasRepository>();
            this.eventAggregatorMock = new Mock<IEventAggregator>();
            this.messageEventMock = new Mock<MessageEvent>();
            this.aliasEventMock = new Mock<AliasEvent>();

            this.eventAggregatorMock.Setup(x => x.GetEvent<MessageEvent>())
                .Returns(messageEventMock.Object);

            this.eventAggregatorMock.Setup(x => x.GetEvent<AliasEvent>())
                .Returns(aliasEventMock.Object);

            this.sut = new AliasService(aliasRepositoryMock.Object, eventAggregatorMock.Object);
        }

        [Test]
        public void NoneMatchingAliasReturnsUntouchedText()
        {
            string input = "name";

            var result = this.sut.GetCommand(input);

            Assert.AreSame(input, result);
        }

        [Test]
        public void WillReturnSimpleCommandOnMatch()
        {
            aliasRepositoryMock.Setup(x => x.GetCommand(It.Is<string>(y => y.Equals("name"))))
                .Returns("command");

            var result = this.sut.GetCommand("name");

            aliasRepositoryMock.Verify(x => x.GetCommand("name"), Times.Once);
            Assert.AreEqual("command", result);
        }

        [Test]
        public void WillResolveCommandInCommandWhenReturned()
        {
            aliasRepositoryMock.Setup(x => x.GetCommand(It.Is<string>(y => y.Equals("name"))))
                .Returns("command;othercommand");
            aliasRepositoryMock.Setup(x => x.GetCommand(It.Is<string>(y => y.Equals("othercommand"))))
                .Returns("extra");

            var result = this.sut.GetCommand("name");

            aliasRepositoryMock.Verify(x => x.GetCommand("name"), Times.Once);
            aliasRepositoryMock.Verify(x => x.GetCommand("othercommand"), Times.Once);
            Assert.AreEqual("command;extra", result);
        }

        [Test]
        public void WillResolveCommandInCommandAndPassingArgument()
        {
            aliasRepositoryMock.Setup(x => x.GetCommand(It.Is<string>(y => y.Equals("name"))))
                .Returns("command %1;othercommand");
            aliasRepositoryMock.Setup(x => x.GetCommand(It.Is<string>(y => y.Equals("othercommand"))))
                .Returns("extra %1");

            var result = this.sut.GetCommand("name target");

            aliasRepositoryMock.Verify(x => x.GetCommand("name"), Times.Once);
            aliasRepositoryMock.Verify(x => x.GetCommand("othercommand"), Times.Once);
            Assert.AreEqual("command target;extra target", result);
        }

        [Test]
        public void WillOnlyReturnCommandsWithArgumentWhenArgumentIdentifier()
        {
            aliasRepositoryMock.Setup(x => x.GetCommand(It.Is<string>(y => y.Equals("name"))))
                .Returns("command;othercommand");
            aliasRepositoryMock.Setup(x => x.GetCommand(It.Is<string>(y => y.Equals("othercommand"))))
                .Returns("extra");

            var result = this.sut.GetCommand("name target");

            aliasRepositoryMock.Verify(x => x.GetCommand("name"), Times.Once);
            aliasRepositoryMock.Verify(x => x.GetCommand("othercommand"), Times.Once);
            Assert.AreEqual("command;extra", result);
        }

        [Test]
        public void WillJoinMultipleArgumentsOnSingleIdentifier()
        {
            aliasRepositoryMock.Setup(x => x.GetCommand(It.Is<string>(y => y.Equals("name"))))
                .Returns("command %1");

            var result = this.sut.GetCommand("name target1 target2 target3");

            aliasRepositoryMock.Verify(x => x.GetCommand("name"), Times.Once);
            Assert.AreEqual("command target1 target2 target3", result);
        }

        [Test]
        public void WillNotJoinMultipleArgumnetsOnSecondIdentifier()
        {
            aliasRepositoryMock.Setup(x => x.GetCommand(It.Is<string>(y => y.Equals("name"))))
                .Returns("command %2");

            var result = this.sut.GetCommand("name target1 target2 target3");

            aliasRepositoryMock.Verify(x => x.GetCommand("name"), Times.Once);
            Assert.AreEqual("command target2", result);
        }

        [Test]
        public void NestedCommandsWillGetExpectedValuesWhenMultipleArgument()
        {
            aliasRepositoryMock.Setup(x => x.GetCommand(It.Is<string>(y => y.Equals("alias1"))))
                .Returns("alias2 %2;alias3");
            aliasRepositoryMock.Setup(x => x.GetCommand(It.Is<string>(y => y.Equals("alias3"))))
                .Returns("alias4");
            aliasRepositoryMock.Setup(x => x.GetCommand(It.Is<string>(y => y.Equals("alias4"))))
                .Returns("alias2 special");

            var result = this.sut.GetCommand("alias1 target1 target2 target3");

            aliasRepositoryMock.Verify(x => x.GetCommand("alias1"), Times.Once);
            aliasRepositoryMock.Verify(x => x.GetCommand("alias3"), Times.Once);
            aliasRepositoryMock.Verify(x => x.GetCommand("alias4"), Times.Once);
            Assert.AreEqual("alias2 target2;alias2 special", result);
        }

        [Test]
        public void TooDeepRecursionWillReturnOriginalTextAndSendSystemMessage()
        {
            string text = "loop";
            MessageBase callback = null;

            messageEventMock.Setup(x => x.Publish(It.IsAny<MessageBase>()))
                .Callback<MessageBase>(c => callback = c);

            aliasRepositoryMock.Setup(x => x.GetCommand(It.Is<string>(y => y.Equals("loop"))))
                .Returns("loop");

            var result = this.sut.GetCommand(text);

            Assert.AreSame(text, result);
            Assert.That(callback, Is.TypeOf<SystemMessage>());
            Assert.AreEqual("Too deep recursion from nested commands at alias: loop", (callback as SystemMessage).Message);
        }

        [Test]
        public void WillReturnOnlyCommandWhenNoParameters()
        {
            aliasRepositoryMock.Setup(x => x.GetCommand(It.Is<string>(y => y.Equals("alias"))))
                .Returns("command %1;othercommand %2 %3");

            var result = this.sut.GetCommand("alias");

            Assert.AreEqual("command;othercommand", result);
        }
    }
}
