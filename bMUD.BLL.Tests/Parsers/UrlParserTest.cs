﻿using bMUD.BLL.Models;
using bMUD.BLL.Parsers;
using NUnit.Framework;
using System;
using System.Windows.Documents;
using System.Windows.Media;

namespace bMUD.BLL.Tests.Parsers
{
    public class UrlParserTest
    {
        [Test]
        public void WhenMessageTextIsNullReturnEmptyResult()
        {
            var message = new ANSIColorText();

            var result = UrlParser.GetUrls(message);

            Assert.IsNull(result);
        }

        [Test]
        public void WhenMessageTextIsEmptyReturnEmptyResult()
        {
            var message = new ANSIColorText { Text = string.Empty };

            var result = UrlParser.GetUrls(message);

            Assert.IsNull(result);
        }

        [Test]
        public void WhenMessageContainsNoUrlReturnMatchingSingleRun()
        {
            var message = new ANSIColorText
            {
                Text = "This is a plain message." + Environment.NewLine,
                Background = new SolidColorBrush(DefaultColor.RED),
                Foreground = new SolidColorBrush(DefaultColor.BLUE)
            };

            var result = UrlParser.GetUrls(message);

            Assert.AreEqual(1, result.Count);
            Assert.That(result[0], Is.TypeOf<Run>());

            var run = result[0] as Run;

            Assert.AreEqual(run.Text, message.Text);
            Assert.AreEqual(run.Background, message.Background);
            Assert.AreEqual(run.Foreground, message.Foreground);
        }

        [Test]
        public void WhenMessageContainsOneUrlReturnThreeMatchingItems()
        {
            var message = new ANSIColorText
            {
                Text = @"This message has http://www.google.se in it." + Environment.NewLine,
                Background = new SolidColorBrush(DefaultColor.RED),
                Foreground = new SolidColorBrush(DefaultColor.BLUE)
            };

            var result = UrlParser.GetUrls(message);

            Assert.AreEqual(3, result.Count);
            Assert.That(result[0], Is.TypeOf<Run>());
            Assert.That(result[1], Is.TypeOf<Hyperlink>());
            Assert.That(result[2], Is.TypeOf<Run>());

            var firstRun = result[0] as Run;
            var link = result[1] as Hyperlink;
            var lastRun = result[2] as Run;

            Assert.AreEqual(firstRun.Text, "This message has ");
            Assert.AreEqual(firstRun.Background, message.Background);
            Assert.AreEqual(firstRun.Foreground, message.Foreground);
            Assert.AreEqual(link.NavigateUri.OriginalString, "http://www.google.se");
            Assert.AreEqual(lastRun.Text, " in it." + Environment.NewLine);
            Assert.AreEqual(lastRun.Background, message.Background);
            Assert.AreEqual(lastRun.Foreground, message.Foreground);
        }

        [Test]
        public void WhenMessageContainsTwoUrlsReturnFiveMatchingItems()
        {
            var message = new ANSIColorText
            {
                Text = @"This message has https://youtube.com and http://www.google.se in it." + Environment.NewLine,
                Background = new SolidColorBrush(DefaultColor.RED),
                Foreground = new SolidColorBrush(DefaultColor.BLUE)
            };

            var result = UrlParser.GetUrls(message);

            Assert.AreEqual(5, result.Count);
            Assert.That(result[0], Is.TypeOf<Run>());
            Assert.That(result[1], Is.TypeOf<Hyperlink>());
            Assert.That(result[2], Is.TypeOf<Run>());
            Assert.That(result[3], Is.TypeOf<Hyperlink>());
            Assert.That(result[4], Is.TypeOf<Run>());

            var firstSection = result[0] as Run;
            var firstLink = result[1] as Hyperlink;
            var secondSection = result[2] as Run;
            var secondLink = result[3] as Hyperlink;
            var thirdSection = result[4] as Run;

            Assert.AreEqual(firstSection.Text, "This message has ");
            Assert.AreEqual(firstSection.Background, message.Background);
            Assert.AreEqual(firstSection.Foreground, message.Foreground);
            Assert.AreEqual(firstLink.NavigateUri.OriginalString, "https://youtube.com");
            Assert.AreEqual(secondSection.Text, " and ");
            Assert.AreEqual(secondSection.Background, message.Background);
            Assert.AreEqual(secondSection.Foreground, message.Foreground);
            Assert.AreEqual(secondLink.NavigateUri.OriginalString, "http://www.google.se");
            Assert.AreEqual(thirdSection.Text, " in it." + Environment.NewLine);
            Assert.AreEqual(thirdSection.Background, message.Background);
            Assert.AreEqual(thirdSection.Foreground, message.Foreground);
        }

        [Test]
        public void WhenMessageContainsThreeUrlsReturnSevenMatchingItems()
        {
            var message = new ANSIColorText
            {
                Text = @"This message has https://youtube.com, http://www.google.se and ftp://filecloud.net in it." + Environment.NewLine,
                Background = new SolidColorBrush(DefaultColor.RED),
                Foreground = new SolidColorBrush(DefaultColor.BLUE)
            };

            var result = UrlParser.GetUrls(message);

            Assert.AreEqual(7, result.Count);
            Assert.That(result[0], Is.TypeOf<Run>());
            Assert.That(result[1], Is.TypeOf<Hyperlink>());
            Assert.That(result[2], Is.TypeOf<Run>());
            Assert.That(result[3], Is.TypeOf<Hyperlink>());
            Assert.That(result[4], Is.TypeOf<Run>());
            Assert.That(result[5], Is.TypeOf<Hyperlink>());
            Assert.That(result[6], Is.TypeOf<Run>());

            var firstSection = result[0] as Run;
            var firstLink = result[1] as Hyperlink;
            var secondSection = result[2] as Run;
            var secondLink = result[3] as Hyperlink;
            var thirdSection = result[4] as Run;
            var thirdLink = result[5] as Hyperlink;
            var fourthSection = result[6] as Run;

            Assert.AreEqual(firstSection.Text, "This message has ");
            Assert.AreEqual(firstSection.Background, message.Background);
            Assert.AreEqual(firstSection.Foreground, message.Foreground);
            Assert.AreEqual(firstLink.NavigateUri.OriginalString, "https://youtube.com");
            Assert.AreEqual(secondSection.Text, ", ");
            Assert.AreEqual(secondSection.Background, message.Background);
            Assert.AreEqual(secondSection.Foreground, message.Foreground);
            Assert.AreEqual(secondLink.NavigateUri.OriginalString, "http://www.google.se");
            Assert.AreEqual(thirdSection.Text, " and ");
            Assert.AreEqual(thirdSection.Background, message.Background);
            Assert.AreEqual(thirdSection.Foreground, message.Foreground);
            Assert.AreEqual(thirdLink.NavigateUri.OriginalString, "ftp://filecloud.net");
            Assert.AreEqual(fourthSection.Text, " in it." + Environment.NewLine);
            Assert.AreEqual(fourthSection.Background, message.Background);
            Assert.AreEqual(fourthSection.Foreground, message.Foreground);
        }

        [Test]
        public void WhenUriFormatExceptionIsThrownFallbackToRun()
        {
            var message = new ANSIColorText
            {
                Text = @"This message has www.wwwwww in it." + Environment.NewLine,
                Background = new SolidColorBrush(DefaultColor.RED),
                Foreground = new SolidColorBrush(DefaultColor.BLUE)
            };

            var result = UrlParser.GetUrls(message);

            Assert.AreEqual(3, result.Count);
            Assert.That(result[0], Is.TypeOf<Run>());
            Assert.That(result[1], Is.TypeOf<Run>());
            Assert.That(result[2], Is.TypeOf<Run>());

            var firstRun = result[0] as Run;
            var fallbackRun = result[1] as Run;
            var lastRun = result[2] as Run;

            Assert.AreEqual(firstRun.Text, "This message has ");
            Assert.AreEqual(firstRun.Background, message.Background);
            Assert.AreEqual(firstRun.Foreground, message.Foreground);
            Assert.AreEqual(fallbackRun.Text, "www.wwwwww");
            Assert.AreEqual(fallbackRun.Background, message.Background);
            Assert.AreEqual(fallbackRun.Foreground, message.Foreground);
            Assert.AreEqual(lastRun.Text, " in it." + Environment.NewLine);
            Assert.AreEqual(lastRun.Background, message.Background);
            Assert.AreEqual(lastRun.Foreground, message.Foreground);
        }
    }
}
