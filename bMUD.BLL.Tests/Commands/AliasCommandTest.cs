﻿using bMUD.BLL.Commands;
using bMUD.Core.Alias;
using bMUD.Core.Events;
using bMUD.Core.Messages;
using Moq;
using NUnit.Framework;
using Prism.Events;

namespace bMUD.BLL.Tests.Commands
{
    public class AliasCommandTest
    {
        Mock<IEventAggregator> eventAggregatorMock;
        Mock<AliasEvent> aliasEventMock;
        Mock<MessageEvent> messageEventMock;

        [SetUp]
        public void AliasCommandTestSetup()
        {
            eventAggregatorMock = new Mock<IEventAggregator>();
            aliasEventMock = new Mock<AliasEvent>();
            messageEventMock = new Mock<MessageEvent>();

            eventAggregatorMock.Setup(x => x.GetEvent<AliasEvent>())
                .Returns(aliasEventMock.Object);

            eventAggregatorMock.Setup(x => x.GetEvent<MessageEvent>())
                .Returns(messageEventMock.Object);
        }

        private AliasCommand CreateSut(string text)
        {
            return new AliasCommand(eventAggregatorMock.Object, text);
        }

        [Test]
        public void ExecuteWillPublishShowAliasDialogWhenEmptyString()
        {
            var sut = CreateSut(" ");
            sut.Execute();

            aliasEventMock.Verify(x => x.Publish(It.IsAny<ShowAliasDialog>()), Times.Once);
        }

        [Test]
        public void ExecuteWillPublishSystemMessageWhenInvalidFormat()
        {
            var sut = CreateSut("this is not a valid format");
            sut.Execute();

            messageEventMock.Verify(x => x.Publish(It.IsAny<SystemMessage>()), Times.Once);
        }

        [Test]
        public void ExecuteWillPublishShowAliasWhenOnlyTarget()
        {
            AliasBase callback = null;

            aliasEventMock.Setup(x => x.Publish(It.IsAny<AliasBase>()))
                .Callback<AliasBase>(c => callback = c);

            var sut = CreateSut("name");
            sut.Execute();

            aliasEventMock.Verify(x => x.Publish(It.IsAny<ShowAlias>()), Times.Once);
            Assert.That(callback, Is.TypeOf<ShowAlias>());
            Assert.AreEqual("name", (callback as ShowAlias).Name);
        }

        [Test]
        public void ExecuteWillPublishAddAliasWhenTargetAndCommand()
        {
            AliasBase callback = null;

            aliasEventMock.Setup(x => x.Publish(It.IsAny<AliasBase>()))
                .Callback<AliasBase>(c => callback = c);

            var sut = CreateSut("name {command that does stuff}");
            sut.Execute();

            aliasEventMock.Verify(x => x.Publish(It.IsAny<AddAlias>()), Times.Once);
            Assert.That(callback, Is.TypeOf<AddAlias>());
            Assert.AreEqual("name", (callback as AddAlias).Name);
            Assert.AreEqual("command that does stuff", (callback as AddAlias).Command);
        }

        [Test]
        public void WillPublishSystemMessageWhenTryingToCreateAliasLoop()
        {
            MessageBase callback = null;

            messageEventMock.Setup(x => x.Publish(It.IsAny<MessageBase>()))
                .Callback<MessageBase>(c => callback = c);

            var sut = CreateSut("name {stuff;and;name}");
            sut.Execute();

            messageEventMock.Verify(x => x.Publish(It.IsAny<SystemMessage>()), Times.Once);
            Assert.AreEqual("Invalid alias, possible recursion.", (callback as SystemMessage).Message);
        }
    }
}
