﻿using bMUD.BLL.Commands;
using bMUD.Core.Alias;
using bMUD.Core.Events;
using bMUD.Core.Messages;
using Moq;
using NUnit.Framework;
using Prism.Events;

namespace bMUD.BLL.Tests.Commands
{
    public class UnAliasCommandTest
    {
        Mock<IEventAggregator> eventAggregatorMock;
        Mock<AliasEvent> aliasEventMock;
        Mock<MessageEvent> messageEventMock;

        [SetUp]
        public void UnAliasCommandTestSetup()
        {
            eventAggregatorMock = new Mock<IEventAggregator>();
            aliasEventMock = new Mock<AliasEvent>();
            messageEventMock = new Mock<MessageEvent>();

            eventAggregatorMock.Setup(x => x.GetEvent<AliasEvent>())
                .Returns(aliasEventMock.Object);

            eventAggregatorMock.Setup(x => x.GetEvent<MessageEvent>())
                .Returns(messageEventMock.Object);
        }

        [Test]
        public void ExecuteWillPublishSystemMessageWhenInvalidFormat()
        {
            var sut = new UnAliasCommand(eventAggregatorMock.Object, "this is not a valid format");
            sut.Execute();

            messageEventMock.Verify(x => x.Publish(It.IsAny<SystemMessage>()), Times.Once);
        }

        [Test]
        public void ExecuteWillPublishRemoveAliasWhenValidString()
        {
            AliasBase callback = null;

            aliasEventMock.Setup(x => x.Publish(It.IsAny<AliasBase>()))
                .Callback<AliasBase>(c => callback = c);

            var sut = new UnAliasCommand(eventAggregatorMock.Object, "name");
            sut.Execute();

            aliasEventMock.Verify(x => x.Publish(It.IsAny<RemoveAlias>()), Times.Once);
            Assert.That(callback, Is.TypeOf<RemoveAlias>());
            Assert.AreEqual("name", (callback as RemoveAlias).Name);
        }
    }
}
