param (
    [string]$SolutionDir,
    [string]$TargetDir
)

$file = "$TargetDir\bMUD.exe"
$ass = [System.Reflection.Assembly]::LoadFile($file)
$v = $ass.GetName().Version;
$version = [string]::Format("{0}.{1}.{2}",$v.Major, $v.Minor, $v.Build)

$publishVersionFolder = "$SolutionDir\Publish\$version"

If(Test-Path $publishVersionFolder) {
    Remove-Item $publishVersionFolder -Recurse
}

New-Item -ItemType Directory -Path $publishVersionFolder

Copy-Item "$TargetDir\*.exe" $publishVersionFolder
Copy-Item "$TargetDir\*.dll" $publishVersionFolder
Copy-Item "$TargetDir\*.config" $publishVersionFolder
Copy-Item "$TargetDir\*.ico" $publishVersionFolder

$ZipTool = Join-Path -path "C:\Program Files\7-Zip" -childpath "7z.exe"

function CreateZip([String] $zipTool, [String] $aDirectory, [String] $aZipfile)
{
    [Array]$arguments = "a", "-tzip", "$aZipfile", "$aDirectory", "-r"
    & $zipTool $arguments
}

$Name = "bMUD_$version.zip"
$zfn = "$SolutionDir\Publish\$Name"

If(Test-Path $zfn) {
    Remove-Item $zfn -Recurse
}

CreateZip $ZipTool (Join-Path -Path $publishVersionFolder -ChildPath "*.*") $zfn

If(Test-Path $publishVersionFolder) {
    Remove-Item $publishVersionFolder -Recurse
}