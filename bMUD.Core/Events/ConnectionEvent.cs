﻿using bMUD.Core.Connections;
using Prism.Events;

namespace bMUD.Core.Events
{
    public class ConnectionEvent : PubSubEvent<ConnectionBase>
    {
    }
}
