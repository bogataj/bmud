﻿using bMUD.Core.Alias;
using Prism.Events;

namespace bMUD.Core.Events
{
    public class AliasEvent : PubSubEvent<AliasBase>
    {
    }
}
