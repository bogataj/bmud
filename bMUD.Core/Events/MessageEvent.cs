﻿using bMUD.Core.Messages;
using Prism.Events;

namespace bMUD.Core.Events
{
    public class MessageEvent : PubSubEvent<MessageBase>
    { }
}
