﻿namespace bMUD.Core.Messages
{
    public class InputMessage : MessageBase
    {
        private readonly bool masked;

        public InputMessage(string message, bool masked = false) : base(message)
        {
            this.masked = masked;
        }

        public bool Masked => this.masked;
    }
}
