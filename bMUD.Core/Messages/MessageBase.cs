﻿namespace bMUD.Core.Messages
{
    public abstract class MessageBase
    {
        private readonly string message;

        public MessageBase(string message)
        {
            this.message = message;
        }

        public virtual string Message => this.message;
    }
}
