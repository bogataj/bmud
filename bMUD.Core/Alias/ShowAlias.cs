﻿namespace bMUD.Core.Alias
{
    public class ShowAlias : AliasBase
    {
        private readonly string name;

        public ShowAlias(string name)
        {
            this.name = name;
        }

        public string Name => this.name;
    }
}
