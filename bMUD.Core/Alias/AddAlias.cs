﻿namespace bMUD.Core.Alias
{
    public class AddAlias : AliasBase
    {
        private readonly string name;
        private readonly string command;

        public AddAlias(string target, string command)
        {
            this.name = target;
            this.command = command;
        }

        public string Name => this.name;
        public string Command => this.command;
    }
}
