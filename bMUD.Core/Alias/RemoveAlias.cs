﻿namespace bMUD.Core.Alias
{
    public class RemoveAlias : AliasBase
    {
        private readonly string name;

        public RemoveAlias(string name)
        {
            this.name = name;
        }

        public string Name => this.name;
    }
}
