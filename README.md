bMUD - bogataj's MUD Client
===========================

For situations when you're on Windows and the only other option available is PuTTY, this will hopefully feel more user friendly while playing a MUD.

A portable client designed for fast and easy use, built on C# .NET 4.6.1 using WPF and [Prism](https://github.com/PrismLibrary/Prism).

Currently contains no configuration, just enter a host and a port and you're ready to play.

** Requires installation of .NET 4.6.1 [[link]](https://www.microsoft.com/en-us/download/details.aspx?id=49981) **

##  Downloads ##
bMUD Version 1.1.1 [Portable ZIP](https://bitbucket.org/bogataj/bmud/downloads/bMUD_1.1.1.zip)

MD5: BFD996F28A93EA111FAD83C9C9CEF94A

## Changelog ##

## 1.1.1 (2017-06-22)

- Minor UI fix connect to a server view.
- Autoscroll will now activate again if the user is sending input, while not currently scrolled to the bottom.

## 1.1.0 (2017-06-21)

- Major enhancements made to scrolling
- Now uses an actual scroll speed implementation, current default value is set to 3.0
- Disabling autoscroll to end, when user manually scrolls upwards
- Implemented support for input delimiter ';'


## 1.0.1 (2017-06-20)

- Multiple bugfixes [[link to issue tracker]](https://bitbucket.org/bogataj/bmud/issues?version=1.0.1)

## 1.0.0 (2017-06-20)

** First official release **

- Basic functionallity, enough to replace PuTTY and other basic telnet clients.
- Keeping track of connection history, for faster reconnecting to a known host.
- Support for masked inputs when server sends no_echo negotiations.
- Smart input history, step between previously sent messages with UP/DOWN-keys.
- Autocomplete, use TAB to iterate through input history matching your current unfinished input.
- ANSI coloring support, using a RGB setup that reassembles [PuTTY](https://en.wikipedia.org/wiki/ANSI_escape_code#Colors)
- URLs are translated to clickable links.

## License ##

bMUD is released as an Open Source software under the [MIT license](https://github.com/nunit/docs/wiki/License). 
License allow the use of bMUD in free and commercial situations without restrictions.

## Credits ##

This application was inspired in its early stages by [MUD Client Essentails](https://www.codeproject.com/Articles/177846/MUD-Client-Essentials) written by [Ryan Hamshire](https://www.codeproject.com/Members/RyanHamshire)