﻿using System.Windows.Media;

namespace bMUD.BLL.Models
{
    public struct ANSIColorText
    {
        public Brush Foreground { get; set; }
        public Brush Background { get; set; }
        public string Text { get; set; }
    }
}
