﻿using System;
using System.ComponentModel;

namespace bMUD.BLL.Models
{
    public class PreviousConnect
    {
        [DisplayName("Host")]
        public String Host { get; set; }

        [DisplayName("Port")]
        public int Port { get; set; }

        [DisplayName("Last Connected")]
        public DateTime LastConnected { get; set; }

        [DisplayName("Session Duration")]
        public String SessionDuration { get; set; }
    }
}
