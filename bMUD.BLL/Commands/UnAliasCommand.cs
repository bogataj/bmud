﻿using bMUD.Core.Alias;
using bMUD.Core.Events;
using bMUD.Core.Messages;
using Prism.Events;
using System;
using System.Text.RegularExpressions;

namespace bMUD.BLL.Commands
{
    public class UnAliasCommand : CommandBase
    {
        private readonly IEventAggregator eventAggregator;
        private readonly string name;

        public UnAliasCommand(IEventAggregator eventAggregator, string arg)
        {
            this.eventAggregator = eventAggregator;

            if (arg != null)
            {
                arg = arg.Trim();

                Regex regex = new Regex(@"^([\d\w_-]+)$");
                Match match = regex.Match(arg);

                if (match.Success)
                {
                    if (match.Groups.Count == 2)
                        this.name = match.Groups[1].Value;
                }
            }
        }

        public override void Execute()
        {
            if (string.IsNullOrEmpty(this.name))
            {
                this.eventAggregator.GetEvent<MessageEvent>()
                    .Publish(new SystemMessage("Invalid syntax, usage:" + Environment.NewLine +
                    "/unalias name            - Remove alias name."));
            }

            else
                this.eventAggregator.GetEvent<AliasEvent>()
                    .Publish(new RemoveAlias(this.name));
        }
    }
}
