﻿namespace bMUD.BLL.Commands
{
    public abstract class CommandBase
    {
        public abstract void Execute();
    }
}
