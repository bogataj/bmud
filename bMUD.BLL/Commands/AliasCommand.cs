﻿using bMUD.Core.Alias;
using bMUD.Core.Events;
using bMUD.Core.Messages;
using Prism.Events;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace bMUD.BLL.Commands
{
    public class AliasCommand : CommandBase
    {
        private readonly IEventAggregator eventAggregator;
        private readonly string name;
        private readonly string command;

        private bool showDialog = false;

        public AliasCommand(IEventAggregator eventAggregator, string arg)
        {
            this.eventAggregator = eventAggregator;

            if (arg != null)
                arg = arg.Trim();

            if (string.IsNullOrEmpty(arg))
                showDialog = true;

            else
            {
                Regex regex = new Regex(@"^([\d\w_-]+)( \{(.+)\})?$");
                Match match = regex.Match(arg);

                if (match.Success)
                {
                    if (match.Groups.Count == 4)
                    {
                        this.name = match.Groups[1].Value;

                        if (match.Groups[3].Value != string.Empty)
                            this.command = match.Groups[3].Value;
                    }
                }
            }
        }

        public override void Execute()
        {
            if (this.showDialog)
                this.eventAggregator.GetEvent<AliasEvent>()
                    .Publish(new ShowAliasDialog());

            else if (string.IsNullOrEmpty(this.name))
            {
                this.eventAggregator.GetEvent<MessageEvent>()
                    .Publish(new SystemMessage("Invalid syntax, usage:" + Environment.NewLine +
                    "/alias                   - Show alias dialog." + Environment.NewLine +
                    "/alias name              - Show stored alias name." + Environment.NewLine +
                    "/alias name {command}    - Add new alias." + Environment.NewLine +
                    "/unalias name            - Remove alias name."));
            }

            else if (string.IsNullOrEmpty(command))
                this.eventAggregator.GetEvent<AliasEvent>()
                    .Publish(new ShowAlias(this.name));

            else
            {
                if (ValidateIntegrityAlias())
                    this.eventAggregator.GetEvent<AliasEvent>()
                        .Publish(new AddAlias(this.name, this.command));
                else
                    this.eventAggregator.GetEvent<MessageEvent>()
                        .Publish(new SystemMessage("Invalid alias, possible recursion."));
            }
        }

        private bool ValidateIntegrityAlias()
        {
            if (!string.IsNullOrEmpty(this.name) && !string.IsNullOrEmpty(this.command))
            {
                if (this.name == this.command)
                    return false;

                return this.command.Split(';').ToList()
                    .FirstOrDefault(x => x.Equals(this.name) || x.StartsWith(this.name + " ")) == null;
            }

            return false;
        }
    }
}
