﻿using bMUD.BLL.Factories;
using bMUD.BLL.Services;
using Microsoft.Practices.Unity;
using Prism.Modularity;

namespace bMUD.BLL
{
    public class BLLModule : IModule
    {
        private readonly IUnityContainer unityContainer;

        public BLLModule(IUnityContainer unityContainer)
        {
            this.unityContainer = unityContainer;
        }

        public void Initialize()
        {
            this.unityContainer.RegisterType<ICommandFactory, CommandFactory>(new ContainerControlledLifetimeManager());
            this.unityContainer.RegisterType<IAliasService, AliasService>(new ContainerControlledLifetimeManager());
            this.unityContainer.RegisterType<IConnectionService, ConnectionService>(new ContainerControlledLifetimeManager());
            this.unityContainer.RegisterType<IInputMemoryService, InputMemoryService>(new ContainerControlledLifetimeManager());
            this.unityContainer.RegisterType<IMacroService, MacroService>(new ContainerControlledLifetimeManager());
        }
    }
}