﻿using bMUD.BLL.Commands;
using Prism.Events;

namespace bMUD.BLL.Factories
{
    public class CommandFactory : ICommandFactory
    {
        private readonly IEventAggregator eventAggregator;

        public CommandFactory(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
        }

        public CommandBase CreateCommand(string str)
        {
            if (string.IsNullOrEmpty(str) || !str.StartsWith("/"))
                return null;

            if (str.Equals("/alias") || str.StartsWith("/alias "))
                return new AliasCommand(this.eventAggregator, str.Substring(6));

            if (str.Equals("/unalias") || str.StartsWith("/unalias "))
                return new UnAliasCommand(this.eventAggregator, str.Substring(8));

            return null;
        }
    }
}
