﻿using bMUD.BLL.Commands;

namespace bMUD.BLL.Factories
{
    public interface ICommandFactory
    {
        CommandBase CreateCommand(string str);
    }
}
