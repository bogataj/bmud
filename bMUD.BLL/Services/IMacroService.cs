﻿using System.Windows.Input;

namespace bMUD.BLL.Services
{
    public interface IMacroService
    {
        bool MatchKeyDown(Key key);
    }
}
