﻿using System;
using System.Windows.Input;

namespace bMUD.BLL.Services
{
    public class MacroService : IMacroService
    {
        private readonly IConnectionService connectionService;

        public MacroService(IConnectionService connectionService)
        {
            this.connectionService = connectionService;
        }

        // Static macros until real implementation...
        public bool MatchKeyDown(Key key)
        {
            switch(key)
            {
                case Key.NumPad0:   this.connectionService.Send("score");   break;
                case Key.NumPad1:   this.connectionService.Send("sw");      break;
                case Key.NumPad2:   this.connectionService.Send("s");       break;
                case Key.NumPad3:   this.connectionService.Send("se");      break;
                case Key.NumPad4:   this.connectionService.Send("w");       break;
                case Key.NumPad5:   this.connectionService.Send("look");    break;
                case Key.NumPad6:   this.connectionService.Send("e");       break;
                case Key.NumPad7:   this.connectionService.Send("nw");      break;
                case Key.NumPad8:   this.connectionService.Send("n");       break;
                case Key.NumPad9:   this.connectionService.Send("ne");      break;
                case Key.Subtract:  this.connectionService.Send("up");      break;
                case Key.Add:       this.connectionService.Send("down");    break;
                case Key.Decimal:   this.connectionService.Send("tin corpse"); break;

                default:
                    return false;
            }

            return true;
        }
    }
}
