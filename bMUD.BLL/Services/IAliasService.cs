﻿namespace bMUD.BLL.Services
{
    public interface IAliasService
    {
        string GetCommand(string text);
    }
}
