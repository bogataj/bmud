﻿using System;

namespace bMUD.BLL.Services
{
    public enum StepDirection
    {
        Forward,
        Backward
    }

    public interface IInputMemoryService
    {
        void RegisterInput(string text);
        String StepInputMemory(StepDirection stepDirection);
        string TryAutoComplete(string text, StepDirection stepDirection);
    }
}
