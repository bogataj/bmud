﻿using bMUD.BLL.Models;
using System.Collections.Generic;

namespace bMUD.BLL.Services
{
    public interface IConnectionService
    {
        void Connect(string hostAdress, int hostPort);
        void Disconnect(bool shuttingDown);
        void Send(string message);

        bool IsConnected { get; }
        bool IsShuttingDown { get; }
        bool MaskEcho { get; }

        IEnumerable<PreviousConnect> GetPreviousConnects();
    }
}
