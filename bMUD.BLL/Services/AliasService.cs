﻿using bMUD.BLL.Extensions;
using bMUD.Core.Alias;
using bMUD.Core.Events;
using bMUD.Core.Messages;
using bMUD.DAL.Repositories;
using Prism.Events;
using System.Text.RegularExpressions;

namespace bMUD.BLL.Services
{
    public class AliasService : IAliasService
    {
        private const int MAX_DEPTH = 50;
        private const int MAX_ARGS = 20;

        private readonly IAliasRepository aliasRepository;
        private readonly IEventAggregator eventAggregator;

        public AliasService(IAliasRepository aliasRepository, IEventAggregator eventAggregator)
        {
            this.aliasRepository = aliasRepository;
            this.eventAggregator = eventAggregator;

            this.eventAggregator.GetEvent<AliasEvent>()
                .Subscribe(AliasRecieved, ThreadOption.BackgroundThread);
        }

        public string GetCommand(string text)
        {
            var textParts = text.Split(' ');
            var aliasName = textParts[0];
            var command = this.aliasRepository.GetCommand(aliasName);

            if (string.IsNullOrEmpty(command))
                return text;

            command = ResolveNestedCommands(command, text, aliasName);
            command = ReplaceArguments(command, textParts);
            command = CleanUnresolvedArguments(command);

            return command;
        }

        private string ResolveNestedCommands(string command, string text, string aliasName)
        {
            int depth = 0;
            bool gotNestedAlias;

            do
            {
                gotNestedAlias = false;

                foreach (var innerAlias in command.Split(';'))
                {
                    var innerCommand = this.aliasRepository.GetCommand(innerAlias.Split(' ')[0]);

                    if (!string.IsNullOrEmpty(innerCommand))
                    {
                        gotNestedAlias = true;
                        command = command.Replace(innerAlias, innerCommand);
                    }
                }
            } while (gotNestedAlias && ++depth < MAX_DEPTH);

            if (depth == MAX_DEPTH)
            {
                this.eventAggregator.GetEvent<MessageEvent>()
                    .Publish(new SystemMessage($"Too deep recursion from nested commands at alias: {aliasName}"));

                return text;
            }

            return command;
        }

        private string ReplaceArguments(string command, string[] textParts)
        {
            var cmdMemory = command;

            for (int i = 1; i < textParts.Length; i++)
            {
                if (!command.Contains($"%{i}"))
                {
                    if (i == 2)
                    {
                        command = cmdMemory.Replace($"%{i - 1}", string.Join(" ", textParts.SubArray((i - 1), textParts.Length - 1)));
                        break;
                    }
                }

                else
                {
                    cmdMemory = command;
                    command = command.Replace($"%{i}", textParts[i]);
                }
            }

            return command;
        }

        private string CleanUnresolvedArguments(string command)
        {
            Regex regex = new Regex(@"\s*%\d+");

            for (int i = 1; i < MAX_ARGS; i++)
                command = regex.Replace(command, "");

            return command;
        }

        private void AliasRecieved(AliasBase obj)
        {
            if (obj.GetType() == typeof(AddAlias))
                AddAlias(obj as AddAlias);
            else if (obj.GetType() == typeof(RemoveAlias))
                RemoveAlias(obj as RemoveAlias);
            else if (obj.GetType() == typeof(ShowAlias))
                ShowAlias(obj as ShowAlias);
        }

        private void AddAlias(AddAlias addAlias)
        {
            if (this.aliasRepository.Add(addAlias.Name, addAlias.Command))
                this.eventAggregator.GetEvent<MessageEvent>()
                    .Publish(new SystemMessage($"Successfully added alias: {addAlias.Name}."));
            else
                this.eventAggregator.GetEvent<MessageEvent>()
                    .Publish(new SystemMessage($"Failed to add alias: {addAlias.Name}."));
        }

        private void RemoveAlias(RemoveAlias removeAlias)
        {
            if (this.aliasRepository.Remove(removeAlias.Name))
                this.eventAggregator.GetEvent<MessageEvent>()
                    .Publish(new SystemMessage($"Successfully removed alias: {removeAlias.Name}."));
            else
                this.eventAggregator.GetEvent<MessageEvent>()
                    .Publish(new SystemMessage($"Failed to remove alias: {removeAlias.Name}."));
        }

        private void ShowAlias(ShowAlias showAlias)
        {
            var command = this.aliasRepository.GetCommand(showAlias.Name);

            if (string.IsNullOrEmpty(command))
                this.eventAggregator.GetEvent<MessageEvent>()
                    .Publish(new SystemMessage($"Found no alias: {showAlias.Name}."));
            else
                this.eventAggregator.GetEvent<MessageEvent>()
                    .Publish(new SystemMessage($"{showAlias.Name}   - {{{command}}}"));
        }
    }
}
