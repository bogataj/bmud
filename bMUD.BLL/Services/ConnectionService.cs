﻿using bMUD.BLL.Factories;
using bMUD.BLL.Helpers;
using bMUD.BLL.Models;
using bMUD.BLL.Parsers;
using bMUD.Core.Connections;
using bMUD.Core.Events;
using bMUD.Core.Messages;
using bMUD.DAL.Repositories;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace bMUD.BLL.Services
{
    public class ConnectionService : IConnectionService
    {
        private readonly IConnectionHistoryRepository historyRepository;
        private readonly IEventAggregator eventAggregator;
        private readonly ICommandFactory commandFactory;
        private readonly IAliasService aliasService;


        private const int BUFFER_SIZE = 1024;
        private readonly byte[] buffer = new byte[BUFFER_SIZE];
        private readonly StringBuilder received;
        private TcpClient connection;
        private bool shuttingDown;
        private bool historyTracking = false;
        private bool maskEcho = false;

        public ConnectionService(IConnectionHistoryRepository historyRepository, IEventAggregator eventAggregator,
            ICommandFactory commandFactory, IAliasService aliasService)
        {
            this.historyRepository = historyRepository;
            this.eventAggregator = eventAggregator;
            this.commandFactory = commandFactory;
            this.aliasService = aliasService;

            shuttingDown = false;
            received = new StringBuilder();
        }

        public bool IsConnected => connection != null && connection.Connected;

        public bool IsShuttingDown => shuttingDown;

        public bool MaskEcho => this.maskEcho;

        public TelnetParser TelnetParser { get; set; }

        public void Connect(string hostAdress, int hostPort)
        {
            try
            {
                if (shuttingDown)
                    return;

                if (IsConnected)
                    Disconnect();

                if (maskEcho)
                    maskEcho = false;

                BroadcastSystemMessage($"Trying to establish connection to: {hostAdress}:{hostPort}{Environment.NewLine}");

                connection = new TcpClient();
                connection.Connect(hostAdress, hostPort);

                if (IsConnected)
                {
                    this.historyRepository.Connect(hostAdress, hostPort);
                    historyTracking = true;

                    BroadcastConnected();

                    TelnetParser = new TelnetParser(connection);

                    connection.Client.BeginReceive(buffer,
                        0,
                        BUFFER_SIZE,
                        SocketFlags.None,
                        new AsyncCallback(HandleServerMessage),
                        null);
                }

                else
                    BroadcastSystemMessage($"Unable to connect!{Environment.NewLine}");
            }

            catch (Exception e)
            {
                BroadcastSystemMessage($"Unable to connect: {e.Message}{Environment.NewLine}");
                return;
            }
        }

        public void Disconnect(bool shuttingDown = false)
        {
            if (shuttingDown)
                this.shuttingDown = shuttingDown;

            if (connection != null)
            {
                if (historyTracking)
                {
                    this.historyRepository.Disconnect();
                    historyTracking = false;
                }

                connection.Close();
                connection = null;
            }

            if (TelnetParser != null)
            {
                TelnetParser.Dispose();
                TelnetParser = null;
            }

            BroadcastDisconnected();
        }

        public void Send(string message)
        {
            var command = this.commandFactory.CreateCommand(message);

            if (command != null)
            {
                BroadcastInputMessage(message);
                command.Execute();

                return;
            }

            if (!IsConnected)
            {
                BroadcastRequestNewConnection();
                return;
            }

            foreach (var messagePart in message.Split(';'))
            {
                var newMessage = this.aliasService.GetCommand(messagePart);

                newMessage += "\r\n";

                Encoder encoder = Encoding.GetEncoding("ISO-8859-1").GetEncoder();
                char[] charArray = newMessage.ToCharArray();
                int count = encoder.GetByteCount(charArray, 0, charArray.Length, true);
                byte[] outputBuffer = new byte[count];

                encoder.GetBytes(charArray, 0, charArray.Length, outputBuffer, 0, true);

                try
                {
                    BroadcastInputMessage(messagePart);

                    if (maskEcho)
                        maskEcho = false;

                    connection.Client.Send(outputBuffer);
                }

                catch (Exception)
                {
                    Disconnect();
                    break;
                }
            }
        }

        private void HandleServerMessage(IAsyncResult ar)
        {
            int receivedBufferSize;

            try
            {
                receivedBufferSize = connection.Client.EndReceive(ar);
            }

            catch (Exception)
            {
                Disconnect();
                return;
            }

            if (receivedBufferSize == 0)
            {
                Disconnect();
                return;
            }

            List<string> telnetMessages;
            List<byte> contentBytes = TelnetParser.HandleAndRemoveTelnetBytes(buffer, receivedBufferSize, out telnetMessages);

            if (telnetMessages.Count > 0 && telnetMessages.Contains("RECV: WILL Echo"))
                maskEcho = true;

            string receivedMessage = ISO88591Decoder.ToUnicode(contentBytes.ToArray(), contentBytes.Count);

            received.Append(receivedMessage);

            if (receivedBufferSize < BUFFER_SIZE)
            {
                BroadcastOutputMessage(received.ToString());
                received.Clear();
            }

            connection.Client.BeginReceive(buffer,
                        0,
                        BUFFER_SIZE,
                        SocketFlags.None,
                        new AsyncCallback(HandleServerMessage),
                        null);
        }

        private void BroadcastConnected()
        {
            eventAggregator.GetEvent<ConnectionEvent>().Publish(new Connected());
        }

        private void BroadcastDisconnected()
        {
            eventAggregator.GetEvent<ConnectionEvent>().Publish(new Disconnected());
        }

        private void BroadcastRequestNewConnection()
        {
            eventAggregator.GetEvent<ConnectionEvent>().Publish(new NewConnection());
        }

        private void BroadcastOutputMessage(string message)
        {
            eventAggregator.GetEvent<MessageEvent>().Publish(new OutputMessage(message));
        }

        private void BroadcastInputMessage(string message)
        {
            eventAggregator.GetEvent<MessageEvent>().Publish(new InputMessage(message, maskEcho));
        }

        private void BroadcastSystemMessage(string message)
        {
            eventAggregator.GetEvent<MessageEvent>().Publish(new SystemMessage(message));
        }

        public IEnumerable<PreviousConnect> GetPreviousConnects()
        {
            var connectionHistory = this.historyRepository.GetHistory();

            return connectionHistory
                .Select(x => new PreviousConnect()
                {
                    Host = x.Host,
                    Port = x.Port,
                    LastConnected = x.Connected,
                    SessionDuration = ParseSessionDuration(x.Disconnected.Subtract(x.Connected))
                }).ToList();
        }

        private string ParseSessionDuration(TimeSpan timeSpan)
        {
            if (timeSpan.Hours < 0 || timeSpan.Minutes < 0 || timeSpan.Seconds < 0)
                return "00:00:00";

            var hours = timeSpan.Hours < 10 ? $"0{timeSpan.Hours}" : timeSpan.Hours.ToString();
            var minutes = timeSpan.Minutes < 10 ? $"0{timeSpan.Minutes}" : timeSpan.Minutes.ToString();
            var seconds = timeSpan.Seconds < 10 ? $"0{timeSpan.Seconds}" : timeSpan.Seconds.ToString();

            return $"{hours}:{minutes}:{seconds}";
        }
    }
}
