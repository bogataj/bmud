﻿using System;
using System.Collections;
using System.Linq;

namespace bMUD.BLL.Services
{
    public class InputMemoryService : IInputMemoryService
    {
        private const int MIN_LENGTH = 3;
        private const int MAX_MEMORY = 200;

        private readonly ArrayList inputMemory;

        private int currentStep;
        private int previousMatchIndex;
        private string previousMatchString;

        public InputMemoryService()
        {
            inputMemory = new ArrayList(MAX_MEMORY);
            currentStep = -1;
            previousMatchIndex = -1;
            previousMatchString = string.Empty;
        }

        public void RegisterInput(string text)
        {
            if (String.IsNullOrEmpty(text))
                return;

            text = text.Trim();

            if (text.Length < MIN_LENGTH)
                return;

            int localIndex;

            if ((localIndex = inputMemory.IndexOf(text)) != -1)
                inputMemory.RemoveAt(localIndex);

            else if (inputMemory.Count == MAX_MEMORY)
                inputMemory.RemoveAt(inputMemory.Count - 1);

            inputMemory.Insert(0, text);
            currentStep = -2;
        }

        public string StepInputMemory(StepDirection stepDirection)
        {
            if (inputMemory.Count == 0)
                return string.Empty;

            if (stepDirection == StepDirection.Forward)
            {
                if (currentStep == -2)
                {
                    currentStep = -1;
                    return string.Empty;
                }

                else if (currentStep == -1)
                    currentStep = inputMemory.Count - 1;

                else if (--currentStep == -1)
                    return string.Empty;
            }

            else if (stepDirection == StepDirection.Backward)
            {
                if (currentStep == -2)
                    currentStep = -1;

                if (++currentStep > (inputMemory.Count - 1))
                {
                    currentStep = -1;
                    return string.Empty;
                }
            }

            return inputMemory[currentStep].ToString();
        }

        public string TryAutoComplete(string text, StepDirection stepDirection)
        {
            if (!previousMatchString.Equals(text))
            {
                previousMatchString = text;
                previousMatchIndex = -1;
            }

            var matches = inputMemory.Cast<string>()
                    .Where(x => x.StartsWith(text))
                    .ToArray();

            if (matches.Length == 0)
                return text;

            if (stepDirection == StepDirection.Forward)
            {
                if (++previousMatchIndex > matches.Length - 1)
                    previousMatchIndex = 0;
            }

            else if (stepDirection == StepDirection.Backward)
            {
                if (--previousMatchIndex <= -1)
                    previousMatchIndex = matches.Length - 1;
            }

            return matches[previousMatchIndex];
        }
    }
}
