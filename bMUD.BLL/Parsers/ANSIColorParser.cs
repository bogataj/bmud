﻿using bMUD.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Media;

namespace bMUD.BLL.Parsers
{
    public static class DefaultColor
    {
        public static Color BLACK { get { return Color.FromRgb(0, 0, 0); } }
        public static Color RED { get { return Color.FromRgb(182, 0, 0); } }
        public static Color GREEN { get { return Color.FromRgb(0, 182, 0); } }
        public static Color YELLOW { get { return Color.FromRgb(182, 182, 0); } }
        public static Color BLUE { get { return Color.FromRgb(0, 0, 182); } }
        public static Color MAGENTA { get { return Color.FromRgb(182, 0, 182); } }
        public static Color CYAN { get { return Color.FromRgb(0, 182, 182); } }
        public static Color GRAY { get { return Color.FromRgb(182, 182, 182); } }

        public static Color B_GRAY { get { return Color.FromRgb(85, 85, 85); } }
        public static Color B_RED { get { return Color.FromRgb(255, 0, 0); } }
        public static Color B_GREEN { get { return Color.FromRgb(0, 255, 0); } }
        public static Color B_YELLOW { get { return Color.FromRgb(255, 255, 0); } }
        public static Color B_BLUE { get { return Color.FromRgb(0, 0, 255); } }
        public static Color B_MAGENTA { get { return Color.FromRgb(255, 0, 255); } }
        public static Color B_CYAN { get { return Color.FromRgb(0, 255, 255); } }

        public static Color WHITE { get { return Color.FromRgb(255, 255, 255); } }
    }

    public class ANSIColorParser
    {
        private readonly string ansiControlRegEx = (char)27 + @"\[" + "[^@-~]*" + "[@-~]";

        private int foregroundColor = 15;
        private int backgroundColor = 0;
        private bool brightColors = false;

        public Color GetColor(int colorNumber)
        {
            switch (colorNumber)
            {
                case 0:
                    return DefaultColor.BLACK;
                case 1:
                    return DefaultColor.RED;
                case 2:
                    return DefaultColor.GREEN;
                case 3:
                    return DefaultColor.YELLOW;
                case 4:
                    return DefaultColor.BLUE;
                case 5:
                    return DefaultColor.MAGENTA;
                case 6:
                    return DefaultColor.CYAN;
                case 7:
                    return DefaultColor.GRAY;
                case 8:
                    return DefaultColor.B_GRAY;
                case 9:
                    return DefaultColor.B_RED;
                case 10:
                    return DefaultColor.B_GREEN;
                case 11:
                    return DefaultColor.B_YELLOW;
                case 12:
                    return DefaultColor.B_BLUE;
                case 13:
                    return DefaultColor.B_MAGENTA;
                case 14:
                    return DefaultColor.B_CYAN;
                default:
                    return DefaultColor.WHITE;
            }
        }

        public List<ANSIColorText> ParseColors(string text)
        {
            List<ANSIColorText> textList = new List<ANSIColorText>();

            if (string.IsNullOrEmpty(text))
                return textList;

            text = text.Replace("\t", "    ");

            int subStartIndex = 0;
            int subEndIndex = text.Length - 1;

            MatchCollection matches = Regex.Matches(text, ansiControlRegEx);

            foreach (Match match in matches)
            {
                string matchValue = match.Value;
                char operationToken = matchValue[matchValue.Length - 1];
                string argsString = matchValue.Substring(2, matchValue.Length - 3);
                string[] argsArray = argsString.Split(';');
                List<int> arguments = new List<int>();

                foreach (string argument in argsArray)
                {
                    try
                    {
                        arguments.Add(int.Parse(argument));
                    }

                    catch (FormatException) { }
                    catch (OverflowException) { }
                }

                if (operationToken.Equals('m'))
                {
                    subEndIndex = match.Index - 1;
                    string subText = text.Substring(subStartIndex, subEndIndex - subStartIndex + 1);

                    ANSIColorText newText = new ANSIColorText();
                    newText.Text = subText;
                    newText.Foreground = new SolidColorBrush(GetColor(foregroundColor));
                    newText.Background = new SolidColorBrush(GetColor(backgroundColor));

                    textList.Add(newText);

                    subStartIndex = match.Index + match.Length;
                    subEndIndex = text.Length - 1;

                    foreach (int param in arguments)
                    {
                        // reset to defaults
                        if (param == 0 || param == 2)
                        {
                            this.brightColors = false;
                            this.foregroundColor = 15;
                            this.backgroundColor = 0;
                        }

                        // bright colors on
                        if (param == 1)
                            this.brightColors = true;

                        // bright colors off
                        else if (param == 22)
                            this.brightColors = false;

                        //s et foreground color
                        else if (param >= 30 && param <= 37)
                        {
                            this.foregroundColor = param - 30;
                            if (this.brightColors) this.foregroundColor += 8;
                        }

                        // set background color
                        else if (param >= 40 && param <= 47)
                        {
                            this.backgroundColor = param - 40;
                            if (this.brightColors) this.backgroundColor += 8;
                        }

                        // default background color
                        else if (param == 49)
                        {
                            this.backgroundColor = 0;
                            if (this.brightColors) this.backgroundColor += 8;
                        }

                        // default foreground color
                        else if (param == 39)
                        {
                            this.foregroundColor = 7;
                            if (this.brightColors) this.foregroundColor += 8;
                        }
                    }
                }

                else
                {
                    ANSIColorText controlSequenceText = new ANSIColorText();
                    controlSequenceText.Text = matchValue;
                    controlSequenceText.Background = new SolidColorBrush(GetColor(14));
                    controlSequenceText.Foreground = new SolidColorBrush(GetColor(0));
                    textList.Add(controlSequenceText);
                }
            }

            int trailingTextStartIndex = 0;

            if (matches.Count > 0)
            {
                Match lastMatch = matches[matches.Count - 1];
                trailingTextStartIndex = lastMatch.Index + lastMatch.Length;
            }

            if (trailingTextStartIndex < text.Length - 1)
            {
                ANSIColorText trailingText = new ANSIColorText();
                trailingText.Text = text.Substring(trailingTextStartIndex);
                trailingText.Foreground = new SolidColorBrush(GetColor(this.foregroundColor));
                trailingText.Background = new SolidColorBrush(GetColor(this.backgroundColor));

                textList.Add(trailingText);
            }

            return textList;
        }
    }
}
