﻿using bMUD.BLL.Models;
using System;
using System.Collections;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows.Documents;

namespace bMUD.BLL.Parsers
{
    public static class UrlParser
    {
        public static ArrayList GetUrls(ANSIColorText message)
        {
            if (String.IsNullOrEmpty(message.Text))
                return null;

            ArrayList list = new ArrayList();
            Regex urlRx = new Regex(@"((https?|ftp)\://|www\.)[A-Za-z0-9\.\-]+(/[A-Za-z0-9\?\&\=;\+!'\(\)\*\-\._~%]*)*", RegexOptions.IgnoreCase);

            MatchCollection matches = urlRx.Matches(message.Text);

            if (matches.Count == 0)
            {
                var run = new Run(message.Text);
                run.Background = message.Background;
                run.Foreground = message.Foreground;

                list.Add(run);
            }

            foreach (Match match in matches)
            {
                if (list.Count == 0)
                    list.Add(CreatePreText(message, match.Value));

                else
                {
                    var previousRun = (list[list.Count - 1] as Run);

                    if (previousRun != null && previousRun.Text.Contains(match.Value))
                        (list[list.Count - 1] as Run).Text = CutAtUrl(previousRun.Text, match.Value);
                }

                try
                {
                    list.Add(CreateHyperlink(match.Value));
                }

                catch(UriFormatException)
                {
                    var run = new Run(match.Value);
                    run.Background = message.Background;
                    run.Foreground = message.Foreground;

                    list.Add(run);
                }

                list.Add(CreatePostText(message, match.Value));
            }

            return list;
        }

        private static string CutAtUrl(string text, string url)
        {
            return text.Substring(0, text.IndexOf(url));
        }

        private static Run CreatePreText(ANSIColorText message, string url)
        {
            var run = new Run(message.Text.Substring(0, message.Text.IndexOf(url)));
            run.Background = message.Background;
            run.Foreground = message.Foreground;

            return run;
        }

        private static Run CreatePostText(ANSIColorText message, string url)
        {
            var run = new Run(message.Text.Substring(message.Text.IndexOf(url) + url.Length));
            run.Background = message.Background;
            run.Foreground = message.Foreground;

            return run;
        }

        private static Hyperlink CreateHyperlink(string url)
        {
            Hyperlink link = new Hyperlink();
            link.IsEnabled = true;
            link.Inlines.Add(url);
            link.NavigateUri = new Uri(url);
            link.RequestNavigate += (sender, args) => Process.Start(args.Uri.ToString());

            return link;
        }
    }
}
