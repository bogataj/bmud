﻿using bMUD.BLL.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace bMUD.BLL.Parsers
{
    enum Telnet : byte
    {
        // Escape
        InterpretAsCommand = 255,

        // Commands
        SubnegotiationEnd = 240,
        NoOperation = 241,
        DataMark = 242,
        Break = 243,
        InterruptProcess = 244,
        AbortOutput = 245,
        AreYouThere = 246,
        EraseCharacter = 247,
        EraseLine = 248,
        GoAhead = 249,
        SubnegotiationBegin = 250,

        // Negotiation
        WILL = 251,
        WONT = 252,
        DO = 253,
        DONT = 254,

        // Options (common)
        SuppressGoAhead = 3,
        Status = 5,
        Echo = 1,
        TimingMark = 6,
        TerminalType = 24,
        TerminalSpeed = 32,
        RemoteFlowControl = 33,
        LineMode = 34,
        EnvironmentVariables = 36,
        NAWS = 31,

        // Options (special)
        MSDP = 69,
        MXP = 91,
        MCCP1 = 85,
        MCCP2 = 86,
        MSP = 90
    };

    public class TelnetParser
    {
        private readonly TcpClient connection;

        public TelnetParser(TcpClient connection)
        {
            this.connection = connection;
        }

        public List<byte> HandleAndRemoveTelnetBytes(byte[] buffer, int receivedCount, out List<string> telnetMessages)
        {
            telnetMessages = new List<string>();
            List<byte> contentBytes = new List<byte>();
            int currentIndex = 0;

            while (currentIndex < receivedCount)
            {
                while (currentIndex < receivedCount && buffer[currentIndex] != (byte)Telnet.InterpretAsCommand)
                {
                    contentBytes.Add(buffer[currentIndex]);
                    currentIndex++;
                }

                if (++currentIndex >= receivedCount)
                    break;

                byte secondByte = buffer[currentIndex];

                if (secondByte == (byte)Telnet.InterpretAsCommand)
                    contentBytes.Add(secondByte);

                else
                {
                    StringBuilder stringVersionOfMessage = new StringBuilder();
                    StringBuilder stringVersionOfResponse = new StringBuilder();

                    if (secondByte == (byte)Telnet.DO)
                    {
                        stringVersionOfMessage.Append("DO ");
                        currentIndex++;

                        if (currentIndex == receivedCount)
                            break;

                        byte thirdByte = buffer[currentIndex];

                        stringVersionOfMessage.Append(InterpretByteAsTelnet(thirdByte));

                        if (thirdByte == (byte)Telnet.NAWS)
                        {
                            stringVersionOfResponse.Append(this.SendTelnetBytes(
                                (byte)Telnet.SubnegotiationBegin, (byte)31,
                                254, 254, 254, 254,
                                (byte)Telnet.InterpretAsCommand, (byte)Telnet.SubnegotiationEnd));
                        }

                        else
                        {
                            stringVersionOfMessage.Append(InterpretByteAsTelnet(thirdByte));
                            stringVersionOfResponse.Append(this.SendTelnetBytes((byte)Telnet.InterpretAsCommand, (byte)Telnet.WONT, thirdByte));
                        }
                    }

                    else if (secondByte == (byte)Telnet.DONT)
                    {
                        stringVersionOfMessage.Append("DONT ");
                        currentIndex++;

                        if (currentIndex == receivedCount)
                            break;

                        byte thirdByte = buffer[currentIndex];

                        stringVersionOfMessage.Append(InterpretByteAsTelnet(thirdByte));
                        stringVersionOfResponse.Append(this.SendTelnetBytes((byte)Telnet.WONT, thirdByte));
                    }

                    else if (secondByte == (byte)Telnet.WILL)
                    {
                        stringVersionOfMessage.Append("WILL ");
                        currentIndex++;

                        if (currentIndex == receivedCount)
                            break;

                        byte thirdByte = buffer[currentIndex];

                        stringVersionOfMessage.Append(InterpretByteAsTelnet(thirdByte));
                        stringVersionOfResponse.Append((this.SendTelnetBytes((byte)Telnet.DONT, thirdByte)));
                    }

                    else if (secondByte == (byte)Telnet.WONT)
                    {
                        stringVersionOfMessage.Append("WONT ");
                        currentIndex++;

                        if (currentIndex == receivedCount)
                            break;

                        byte thirdByte = buffer[currentIndex];

                        stringVersionOfMessage.Append(InterpretByteAsTelnet(thirdByte));
                        stringVersionOfResponse.Append(this.SendTelnetBytes((byte)Telnet.DONT, thirdByte));
                    }

                    else if (secondByte == (byte)Telnet.SubnegotiationBegin)
                    {
                        stringVersionOfMessage.Append("SB ");
                        List<byte> subnegotiationBytes = new List<byte>();

                        while (currentIndex < receivedCount - 1 &&
                            !(buffer[currentIndex] == (byte)Telnet.InterpretAsCommand && buffer[currentIndex] == (byte)Telnet.SubnegotiationEnd))
                        {
                            subnegotiationBytes.Add(buffer[currentIndex]);
                            currentIndex++;
                        }

                        byte[] subnegotiationBytesArray = subnegotiationBytes.ToArray();

                        stringVersionOfMessage.Append(ISO88591Decoder.ToUnicode(subnegotiationBytesArray, subnegotiationBytes.Count));
                        stringVersionOfMessage.Append(" SE");
                    }

                    else
                        stringVersionOfMessage.Append(InterpretByteAsTelnet(secondByte));

                    string messageToReport = stringVersionOfMessage.ToString();

                    if (!string.IsNullOrEmpty(messageToReport))
                        telnetMessages.Add("RECV: " + messageToReport.ToString());

                    string responseToReport = stringVersionOfResponse.ToString();

                    if (!string.IsNullOrEmpty(responseToReport))
                        telnetMessages.Add("SEND: " + stringVersionOfResponse.ToString());
                }

                currentIndex++;
            }

            return contentBytes;
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        private string InterpretByteAsTelnet(byte thisByte)
        {
            string friendlyName = Enum.GetName(typeof(Telnet), thisByte);

            if (string.IsNullOrEmpty(friendlyName))
                friendlyName = '[' + thisByte.ToString() + ']';

            return friendlyName;
        }

        private string SendTelnetBytes(params byte[] bytes)
        {
            if (!connection.Connected)
                return "";

            connection.Client.Send(new byte[] { (byte)Telnet.InterpretAsCommand });
            connection.Client.Send(bytes);

            StringBuilder reportMessage = new StringBuilder();

            for (int i = 0; i < bytes.Length; i++)
                reportMessage.Append(this.InterpretByteAsTelnet(bytes[i]) + " ");

            return reportMessage.ToString();
        }
    }
}
