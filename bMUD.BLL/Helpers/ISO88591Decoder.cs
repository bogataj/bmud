﻿using System.Text;

namespace bMUD.BLL.Helpers
{
    public static class ISO88591Decoder
    {
        public static string ToUnicode(byte[] bytes, int lengthToConvert)
        {
            Decoder decoder = Encoding.GetEncoding("ISO-8859-1").GetDecoder();
            char[] chars = new char[lengthToConvert];
            int charLen = decoder.GetChars(bytes, 0, lengthToConvert, chars, 0);

            return new string(chars);
        }
    }
}
