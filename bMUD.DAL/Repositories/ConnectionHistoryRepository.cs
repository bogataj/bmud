﻿using bMUD.DAL.Config;
using bMUD.DAL.Entities;
using LiteDB;
using System;
using System.Collections.Generic;

namespace bMUD.DAL.Repositories
{
    public class ConnectionHistoryRepository : IConnectionHistoryRepository
    {
        private ConnectionHistory currentHistory;

        public void Connect(string host, int port)
        {
            using (var db = new LiteRepository(DatabaseConfig.DatabaseName))
            {
                var entity = db.SingleOrDefault<ConnectionHistory>(x => x.Host == host && x.Port == port);

                if (entity == null)
                {
                    entity = new ConnectionHistory
                    {
                        Host = host,
                        Port = port,
                        Connected = DateTime.Now
                    };

                    db.Insert(entity);
                }

                else
                {
                    entity.Connected = DateTime.Now;
                    db.Update(entity);
                }

                currentHistory = entity;
            }
        }

        public void Disconnect()
        {
            using (var db = new LiteRepository(DatabaseConfig.DatabaseName))
            {
                if (currentHistory != null)
                {
                    currentHistory.Disconnected = DateTime.Now;
                    db.Update(currentHistory);

                    currentHistory = null;
                }
            }
        }

        public IEnumerable<ConnectionHistory> GetHistory()
        {
            using (var db = new LiteRepository(DatabaseConfig.DatabaseName))
            {
                var entity = db.Query<ConnectionHistory>()
                    .Where(Query.All("Connected", Query.Descending))
                    .ToList();

                return entity;
            }
        }
    }
}
