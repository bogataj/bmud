﻿using bMUD.DAL.Entities;
using System.Collections.Generic;

namespace bMUD.DAL.Repositories
{
    public interface IConnectionHistoryRepository
    {
        IEnumerable<ConnectionHistory> GetHistory();
        void Connect(string host, int port);
        void Disconnect();
    }
}
