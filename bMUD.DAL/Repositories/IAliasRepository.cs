﻿namespace bMUD.DAL.Repositories
{
    public interface IAliasRepository
    {
        string GetCommand(string name);
        bool Add(string name, string command);
        bool Remove(string name);
    }
}
