﻿using bMUD.DAL.Config;
using bMUD.DAL.Entities;
using LiteDB;

namespace bMUD.DAL.Repositories
{
    public class AliasRepository : IAliasRepository
    {
        public bool Add(string name, string command)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(command))
                return false;

            using (var db = new LiteRepository(DatabaseConfig.DatabaseName))
            {
                var currentAlias = db.FirstOrDefault<Alias>(x => x.Name.Equals(name));

                if (currentAlias != null)
                {
                    currentAlias.Command = command;
                    db.Update(currentAlias);
                }

                else
                {
                    var alias = new Alias
                    {
                        Name = name,
                        Command = command
                    };

                    db.Insert(alias);
                }

                return true;
            }
        }

        public string GetCommand(string name)
        {
            using (var db = new LiteRepository(DatabaseConfig.DatabaseName))
            {
                return db.FirstOrDefault<Alias>(x => x.Name.Equals(name))?.Command;
            }
        }

        public bool Remove(string name)
        {
            if (string.IsNullOrEmpty(name))
                return false;

            using (var db = new LiteRepository(DatabaseConfig.DatabaseName))
            {
                var alias = db.FirstOrDefault<Alias>(x => x.Name.Equals(name));

                if (alias != null)
                    return db.Delete<Alias>(alias.Id);

                return false;
            }
        }
    }
}
