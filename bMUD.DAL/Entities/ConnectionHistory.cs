﻿using System;

namespace bMUD.DAL.Entities
{
    public class ConnectionHistory
    {
        public int Id { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public DateTime Connected { get; set; }
        public DateTime Disconnected { get; set; }
    }
}
