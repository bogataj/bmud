﻿namespace bMUD.DAL.Entities
{
    public class Alias
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Command { get; set; }
    }
}
