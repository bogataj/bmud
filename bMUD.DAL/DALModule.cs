﻿using bMUD.DAL.Repositories;
using Microsoft.Practices.Unity;
using Prism.Modularity;

namespace bMUD.DAL
{
    public class DALModule : IModule
    {
        private readonly IUnityContainer unityContainer;

        public DALModule(IUnityContainer unityContainer)
        {
            this.unityContainer = unityContainer;
        }

        public void Initialize()
        {
            this.unityContainer.RegisterType<IConnectionHistoryRepository, ConnectionHistoryRepository>(new ContainerControlledLifetimeManager());
            this.unityContainer.RegisterType<IAliasRepository, AliasRepository>(new ContainerControlledLifetimeManager());
        }
    }
}
