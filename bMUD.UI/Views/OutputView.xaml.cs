﻿using bMUD.Core.Connections;
using bMUD.Core.Events;
using bMUD.UI.Controllers;
using Prism.Events;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace bMUD.UI.Views
{
    /// <summary>
    /// Interaction logic for OutputView.xaml
    /// </summary>
    public partial class OutputView : UserControl
    {
        private readonly IInputFocusController inputFocusController;
        private readonly IEventAggregator eventAggregator;

        private bool connected = false;

        public OutputView(IInputFocusController inputFocusController, IEventAggregator eventAggregator)
        {
            this.inputFocusController = inputFocusController;
            this.eventAggregator = eventAggregator;

            InitializeComponent();

            DataObject.AddCopyingHandler(flowDocument, OnCopy);

            this.eventAggregator.GetEvent<ConnectionEvent>()
                .Subscribe(ConnectionRecieved, ThreadOption.BackgroundThread);
        }

        private void ConnectionRecieved(ConnectionBase obj)
        {
            if (obj.GetType() == typeof(Connected))
                connected = true;

            else if (obj.GetType() == typeof(Disconnected))
                connected = false;
        }

        private void OnCopy(object sender, DataObjectCopyingEventArgs e)
        {
            (e.Source as FlowDocumentScrollViewer).Selection.Select(flowDocument.Selection.Start, flowDocument.Selection.Start);

            this.inputFocusController.SetFocusTarget();
        }

        private void flowDocument_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if ((flowDocument.Selection.IsEmpty))
            {
                this.inputFocusController.SetFocusTarget();

                if (!connected)
                    this.eventAggregator.GetEvent<ConnectionEvent>().Publish(new NewConnection());
            }
        }
    }
}
