﻿using bMUD.UI.ViewModels;
using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Input;

namespace bMUD.UI.Views
{
    /// <summary>
    /// Interaction logic for ConnectView.xaml
    /// </summary>
    public partial class ConnectView : UserControl
    {
        private ConnectViewModel viewModel;

        public ConnectView()
        {
            InitializeComponent();
        }

        private ConnectViewModel ViewModel
        {
            get
            {
                if (viewModel == null)
                    viewModel = (DataContext as ConnectViewModel);

                return viewModel;
            }
        }

        private void DataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            var displayName = GetPropertyDisplayName(e.PropertyDescriptor);

            if (!string.IsNullOrEmpty(displayName))
                e.Column.Header = displayName;
        }

        private static string GetPropertyDisplayName(object descriptor)
        {
            var propertyDescriptor = descriptor as PropertyDescriptor;

            if (propertyDescriptor != null)
            {
                var displayName = propertyDescriptor.Attributes[typeof(DisplayNameAttribute)] as DisplayNameAttribute;

                if (displayName != null && displayName != DisplayNameAttribute.Default)
                    return displayName.DisplayName;
            }

            else
            {
                var propertyInfo = descriptor as PropertyInfo;

                if (propertyInfo != null)
                {
                    Object[] attributes = propertyInfo.GetCustomAttributes(typeof(DisplayNameAttribute), true);

                    for (int i = 0; i < attributes.Length; ++i)
                    {
                        var displayName = attributes[i] as DisplayNameAttribute;

                        if (displayName != null && displayName != DisplayNameAttribute.Default)
                            return displayName.DisplayName;
                    }
                }
            }

            return null;
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && e.IsDown)
                ViewModel.OkCommand.Execute();
        }
    }
}
