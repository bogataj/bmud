﻿using System.Windows;
using System.Windows.Controls;
using Prism.Regions;
using bMUD.BLL.Services;

namespace bMUD.UI.Views
{
    /// <summary>
    /// Interaction logic for WorkspaceView.xaml
    /// </summary>
    public partial class WorkspaceView : UserControl, INavigationAware
    {
        private bool initialized = false;
        private readonly IConnectionService connectionService;

        public WorkspaceView(IConnectionService connectionService)
        {
            this.connectionService = connectionService;

            InitializeComponent();
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        { }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            if (!initialized)
            {
                Window.GetWindow(this).Closing += W_Closing;
                initialized = true;
            }
        }

        private void W_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.connectionService?.IsConnected == true)
                this.connectionService.Disconnect(true);
        }
    }
}
