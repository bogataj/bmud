﻿using System.Windows.Controls;
using System.Windows.Input;
using bMUD.UI.ViewModels;
using bMUD.UI.Controllers;
using System.Windows;

namespace bMUD.UI.Views
{
    /// <summary>
    /// Interaction logic for InputView.xaml
    /// </summary>
    public partial class InputView : UserControl
    {
        private readonly IInputFocusController inputFocusController;
        private InputViewModel viewModel;

        public InputView(IInputFocusController inputFocusController)
        {
            InitializeComponent();

            this.inputFocusController = inputFocusController;
            this.inputFocusController.SetFocus += InputFocusController_SetFocus;
        }

        private InputViewModel ViewModel
        {
            get
            {
                if (viewModel == null)
                    viewModel = (DataContext as InputViewModel);

                return viewModel;
            }
        }

        private void InputFocusController_SetFocus()
        {
            this.inputTextBox.Focus();
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (ViewModel != null)
            {
                if (e.IsDown)
                {
                    if (ViewModel.HandleKeyDown(e.Key, inputTextBox))
                        e.Handled = true;
                }
            }
        }

        private void TextBox_Loaded(object sender, RoutedEventArgs e)
        {
            MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
        }
    }
}
