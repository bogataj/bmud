﻿using bMUD.UI.Controllers;
using bMUD.UI.Views;
using Microsoft.Practices.Unity;
using Prism.Modularity;
using Prism.Regions;
using Prism.Unity;

namespace bMUD.UI
{
    public class UIModule : IModule
    {
        private readonly IUnityContainer unityContainer;
        private readonly IRegionManager regionManager;

        public UIModule(IRegionManager regionManager, IUnityContainer unityContainer)
        {
            this.regionManager = regionManager;
            this.unityContainer = unityContainer;
        }

        public void Initialize()
        {
            this.unityContainer.RegisterType<IInputFocusController, InputFocusController>(new ContainerControlledLifetimeManager());

            this.unityContainer.RegisterTypeForNavigation<WorkspaceView>();
            this.unityContainer.RegisterTypeForNavigation<InputView>();
            this.unityContainer.RegisterTypeForNavigation<OutputView>();

            this.regionManager.RegisterViewWithRegion("WorkspaceRegion",
                () => this.unityContainer.Resolve<WorkspaceView>());
            this.regionManager.RegisterViewWithRegion("InputRegion",
                () => this.unityContainer.Resolve<InputView>());
            this.regionManager.RegisterViewWithRegion("OutputRegion",
                () => this.unityContainer.Resolve<OutputView>());
        }
    }
}