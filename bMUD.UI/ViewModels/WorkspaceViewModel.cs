﻿using bMUD.BLL.Services;
using bMUD.Core.Connections;
using bMUD.Core.Events;
using bMUD.UI.Notifications;
using Prism.Events;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using Prism.Regions;
using System;

namespace bMUD.UI.ViewModels
{
    public class WorkspaceViewModel : BindableBase, INavigationAware
    {
        private readonly IConnectionService connectionService;

        public WorkspaceViewModel(IConnectionService connectionService, IEventAggregator eventAggregator)
        {
            this.connectionService = connectionService;

            eventAggregator.GetEvent<ConnectionEvent>()
                .Subscribe(ConnectionReceived, ThreadOption.UIThread, true,
                    (filter) => filter.GetType() == typeof(NewConnection) || filter.GetType() == typeof(Disconnected));

            ConnectNotificationRequest = new InteractionRequest<ConnectNotification>();
        }

        public InteractionRequest<ConnectNotification> ConnectNotificationRequest { get; set; }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        { }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            if (!this.connectionService.IsConnected)
                RaiseConnectNotification();
        }

        private void ConnectionReceived(ConnectionBase obj)
        {
            RaiseConnectNotification();
        }

        private void RaiseConnectNotification()
        {
            int port = 0;

            if(!this.connectionService.IsConnected && !this.connectionService.IsShuttingDown)
            {
                ConnectNotificationRequest.Raise(new ConnectNotification { Title = "Connect to a server" }, x =>
                {
                    if (x.Confirmed)
                    {
                        if (Int32.TryParse(x.Port, out port))
                            this.connectionService.Connect(x.Host, port);
                    }
                });
            }
        }
    }
}
