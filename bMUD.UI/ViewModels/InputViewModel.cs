﻿using bMUD.BLL.Services;
using Prism.Mvvm;
using System;
using System.Windows.Input;
using System.Windows.Controls;

namespace bMUD.UI.ViewModels
{
    public class InputViewModel : BindableBase
    {
        private readonly IConnectionService connectionService;
        private readonly IInputMemoryService inputMemoryService;
        private readonly IMacroService macroService;

        private string inputText = string.Empty;
        private string maskedText = string.Empty;
        private string autoCompleteMemory = string.Empty;
        private bool autoCompleteMode = false;

        public InputViewModel(IConnectionService connectionService, IInputMemoryService inputMemoryService,
            IMacroService macroService)
        {
            this.connectionService = connectionService;
            this.inputMemoryService = inputMemoryService;
            this.macroService = macroService;
        }

        public void Send()
        {
            if (this.connectionService.MaskEcho)
            {
                this.connectionService.Send(this.maskedText);
                this.maskedText = string.Empty;
                InputText = string.Empty;
            }
            else
            {
                if (!autoCompleteMode && autoCompleteMemory.Length > 0)
                    autoCompleteMemory = string.Empty;

                this.inputMemoryService.RegisterInput(this.inputText);
                this.connectionService.Send(this.inputText);
            }
        }

        internal bool HandleKeyDown(Key key, TextBox inputTextBox)
        {
            bool keyHandeled = true;

            switch (key)
            {
                case Key.Enter:
                    Send();
                    inputTextBox.SelectAll();
                    break;
                case Key.Up:
                    GetPreviousInput();
                    inputTextBox.CaretIndex = inputTextBox.Text.Length;
                    inputTextBox.ScrollToEnd();
                    break;
                case Key.Down:
                    GetNextInput();
                    inputTextBox.CaretIndex = inputTextBox.Text.Length;
                    inputTextBox.ScrollToEnd();
                    break;
                case Key.Tab:
                    if ((Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift)
                        PreviousAutoComplete();
                    else
                        NextAutoComplete();
                    inputTextBox.CaretIndex = inputTextBox.Text.Length;
                    inputTextBox.ScrollToEnd();
                    break;
                default:
                    keyHandeled = false;
                    break;
            }

            if (this.macroService.MatchKeyDown(key))
                keyHandeled = true;

            return keyHandeled;
        }

        public string InputText
        {
            get { return this.inputText; }
            set
            {
                if (this.connectionService.MaskEcho)
                {
                    if (value.Length < maskedText.Length)
                        maskedText = maskedText.Substring(0, maskedText.Length - 1);

                    else
                    {
                        if (!string.IsNullOrEmpty(value))
                        {
                            maskedText += value[value.Length - 1];
                            value = new string('*', value.Length);
                        }
                    }
                }

                if (!autoCompleteMode && autoCompleteMemory.Length > 0)
                    autoCompleteMemory = string.Empty;

                SetProperty<string>(ref this.inputText, value);
                this.RaisePropertyChanged(nameof(InputText));
            }
        }

        public void NextAutoComplete()
        {
            TryAutoComplete(StepDirection.Forward);
        }

        public void PreviousAutoComplete()
        {
            TryAutoComplete(StepDirection.Backward);
        }

        private void TryAutoComplete(StepDirection stepDirection)
        {
            if (String.IsNullOrEmpty(this.inputText))
                return;

            if (!this.connectionService.MaskEcho)
            {
                if (String.IsNullOrEmpty(autoCompleteMemory))
                    autoCompleteMemory = InputText;

                var match = this.inputMemoryService.TryAutoComplete(autoCompleteMemory, stepDirection);

                if (this.inputText.Equals(match))
                    return;

                autoCompleteMode = true;
                InputText = match;
                autoCompleteMode = false;
            }
        }

        public void GetNextInput()
        {
            if (!this.connectionService.MaskEcho)
                InputText = this.inputMemoryService.StepInputMemory(StepDirection.Forward);
        }

        public void GetPreviousInput()
        {
            if (!this.connectionService.MaskEcho)
                InputText = this.inputMemoryService.StepInputMemory(StepDirection.Backward);
        }
    }
}
