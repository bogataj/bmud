﻿using bMUD.BLL.Models;
using bMUD.BLL.Services;
using bMUD.UI.Notifications;
using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace bMUD.UI.ViewModels
{
    public class ConnectViewModel : BindableBase, IInteractionRequestAware
    {
        private readonly IConnectionService connectionService;
        private ConnectNotification request;
        private PreviousConnect selectedConnection;

        public ConnectViewModel(IConnectionService connectionService)
        {
            this.connectionService = connectionService;

            OkCommand = new DelegateCommand(OnOk);
            CloseCommand = new DelegateCommand(OnClose);
        }

        public DelegateCommand OkCommand { get; }
        public DelegateCommand CloseCommand { get; }
        public Action FinishInteraction { get; set; }
        public IEnumerable<PreviousConnect> PreviousConnections => this.connectionService.GetPreviousConnects()
                .OrderByDescending(x => x.LastConnected);

        public string Host
        {
            get => request?.Host ?? string.Empty;
            set
            {
                if (request == null)
                    return;

                request.Host = value;
                RaisePropertyChanged(nameof(Host));
            }
        }

        public string Port
        {
            get => request?.Port ?? string.Empty;
            set
            {
                if (request == null)
                    return;

                request.Port = value;
                RaisePropertyChanged(nameof(Port));
            }
        }

        public INotification Notification
        {
            get => request;
            set
            {
                SetProperty(ref request, value as ConnectNotification);
                RaisePropertyChanged(nameof(Host));
                RaisePropertyChanged(nameof(Port));
                RaisePropertyChanged(nameof(PreviousConnections));
            }
        }

        public PreviousConnect SelectedConnection
        {
            get => this.selectedConnection;
            set
            {
                SetProperty(ref this.selectedConnection, value);
                RaisePropertyChanged(nameof(SelectedConnection));

                if (this.selectedConnection != null)
                {
                    Host = this.selectedConnection.Host;
                    Port = this.selectedConnection.Port.ToString();
                }
            }
        }

        private void OnOk()
        {
            if (request != null)
                request.Confirmed = true;

            SelectedConnection = null;

            FinishInteraction();
        }

        private void OnClose()
        {
            FinishInteraction();
        }
    }
}
