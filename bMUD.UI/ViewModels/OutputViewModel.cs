﻿using bMUD.BLL.Models;
using bMUD.BLL.Parsers;
using bMUD.Core.Connections;
using bMUD.Core.Events;
using bMUD.Core.Messages;
using bMUD.UI.Controllers;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace bMUD.UI.ViewModels
{
    public class OutputViewModel : BindableBase
    {
        private const double ScrollSpeed = 3.0;

        private readonly IInputFocusController inputFocusController;
        private readonly ANSIColorParser colorParser = new ANSIColorParser();

        private ScrollViewer descendantScrollViewer;
        private FlowDocument outputFlow;
        private bool autoScroll = true;

        public OutputViewModel(IInputFocusController inputFocusController, IEventAggregator eventAggregator)
        {
            this.inputFocusController = inputFocusController;

            eventAggregator.GetEvent<ConnectionEvent>()
                .Subscribe(ConnectionRecieved, ThreadOption.UIThread, true,
                (filter) => filter.GetType() == typeof(Connected) || filter.GetType() == typeof(Disconnected));

            eventAggregator.GetEvent<MessageEvent>()
                .Subscribe(MessageReceived, ThreadOption.UIThread);

            var _outputFlow = new FlowDocument();

            _outputFlow.Background = new SolidColorBrush(Color.FromRgb(0, 0, 0));
            _outputFlow.PagePadding = new Thickness(3);

            Style style = new Style(typeof(Paragraph));
            style.Setters.Add(new Setter(TextElement.FontFamilyProperty, new FontFamily("Courier New")));
            style.Setters.Add(new Setter(Block.PaddingProperty, new Thickness(0)));
            style.Setters.Add(new Setter(Block.MarginProperty, new Thickness(0)));

            _outputFlow.Resources.Add(typeof(Paragraph), style);
            _outputFlow.Blocks.Add(new Paragraph());

            OutputFlow = _outputFlow;
            ScrollToEndCommand = new DelegateCommand(OnScrollToEnd);
        }

        public DelegateCommand ScrollToEndCommand { get; }

        public FlowDocument OutputFlow
        {
            get { return this.outputFlow; }
            set
            {
                this.SetProperty(ref this.outputFlow, value);
                this.RaisePropertyChanged(nameof(OutputFlow));
            }
        }

        public bool AutoScroll
        {
            get => this.autoScroll;

            set
            {
                SetProperty(ref this.autoScroll, value);
                RaisePropertyChanged(nameof(AutoScroll));
            }
        }

        private ScrollViewer DescendantScrollViewer
        {
            get
            {
                if (descendantScrollViewer == null)
                {
                    descendantScrollViewer = FindScrollViewerDescendant(OutputFlow.Parent);

                    if (descendantScrollViewer != null)
                    {
                        descendantScrollViewer.ScrollChanged += ScrollViewer_ScrollChanged;
                        descendantScrollViewer.PreviewMouseWheel += new MouseWheelEventHandler(OnPreviewMouseWheelScrolled);
                        OutputFlow.PreviewMouseWheel += OutputFlow_PreviewMouseWheel;
                    }
                }

                return descendantScrollViewer;
            }
        }

        private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;

            if (scrollViewer != null)
            {
                e.Handled = true;

                if (e.ExtentHeightChange == 0)
                {
                    if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                        AutoScroll = true;
                    else
                        AutoScroll = false;
                }

                if (AutoScroll && e.ExtentHeightChange != 0)
                    scrollViewer.ScrollToVerticalOffset(scrollViewer.ExtentHeight);
            }
        }

        private void OutputFlow_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;

            OnPreviewMouseWheelScrolled(DescendantScrollViewer, e);
        }

        private static void OnPreviewMouseWheelScrolled(object sender, MouseWheelEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;

            if (scrollViewer != null)
            {
                double offset = scrollViewer.VerticalOffset - (e.Delta * ScrollSpeed / 6);

                if (offset < 0)
                    scrollViewer.ScrollToVerticalOffset(0);

                else if (offset > scrollViewer.ExtentHeight)
                    scrollViewer.ScrollToVerticalOffset(scrollViewer.ExtentHeight);

                else
                    scrollViewer.ScrollToVerticalOffset(offset);

                e.Handled = true;
            }
        }

        private void MessageReceived(MessageBase obj)
        {
            if (obj.GetType() == typeof(OutputMessage))
                CreateOutputMessage(obj.Message);

            else if (obj.GetType() == typeof(InputMessage))
            {
                var msg = obj as InputMessage;
                CreateInputMessage(msg.Message, msg.Masked);
            }
            else
                CreateSystemMessage(obj.Message);
        }

        private void ConnectionRecieved(ConnectionBase obj)
        {
            string message = null;

            if (obj.GetType() == typeof(Connected))
                message = $"Connected!{Environment.NewLine}";
            else if (obj.GetType() == typeof(Disconnected))
                message = $"Connection has been lost!{Environment.NewLine}";

            CreateSystemMessage(message);
        }

        private void CreateSystemMessage(string message)
        {
            if (String.IsNullOrEmpty(message))
                return;

            if (!message.EndsWith(Environment.NewLine))
                message = message + Environment.NewLine;

            Run run = new Run(message);
            run.Foreground = new SolidColorBrush(Colors.SkyBlue);
            run.Background = new SolidColorBrush(Colors.Black);

            AppendRun(run);
        }

        private void CreateInputMessage(string message, bool maskEcho)
        {
            if (String.IsNullOrEmpty(message))
                return;

            if (maskEcho)
            {
                if (message.Trim().Length > 0)
                    message = new String('*', message.Length) + Environment.NewLine;
            }

            if (!message.EndsWith(Environment.NewLine))
                message = message + Environment.NewLine;

            Run run = new Run(message);
            run.Foreground = new SolidColorBrush(Colors.Gold);
            run.Background = new SolidColorBrush(Colors.Black);

            AppendRun(run);

            if (!AutoScroll)
            {
                Application.Current.Dispatcher.Invoke(new Action(delegate
                {
                    ScrollToEndCommand.Execute();
                }), System.Windows.Threading.DispatcherPriority.ContextIdle);
            }
        }

        private void CreateOutputMessage(string message)
        {
            if (String.IsNullOrEmpty(message))
                return;

            List<ANSIColorText> textList = colorParser.ParseColors(message);
            ArrayList textSegments = new ArrayList();

            foreach (var text in textList)
            {
                ArrayList withUrls = UrlParser.GetUrls(text);

                if (withUrls?.Count > 0)
                    textSegments.AddRange(withUrls);
            }

            if (textSegments.Count > 0)
                AppendRuns(textSegments);
        }

        private void AppendRun(Run run)
        {
            (OutputFlow.Blocks.FirstBlock as Paragraph).Inlines.Add(run);

            DescendantScrollViewer?.ScrollToLeftEnd();
        }

        private void AppendRuns(ArrayList textSegments)
        {
            (OutputFlow.Blocks.FirstBlock as Paragraph).Inlines.AddRange(textSegments);

            DescendantScrollViewer?.ScrollToLeftEnd();
        }

        private ScrollViewer FindScrollViewerDescendant(DependencyObject control)
        {
            if (control is ScrollViewer)
                return (control as ScrollViewer);

            int childCount = VisualTreeHelper.GetChildrenCount(control);

            for (int i = 0; i < childCount; i++)
            {
                ScrollViewer result = FindScrollViewerDescendant(VisualTreeHelper.GetChild(control, i));

                if (result != null)
                    return result;
            }

            return null;
        }

        private void OnScrollToEnd()
        {
            DescendantScrollViewer.ScrollToEnd();

            this.inputFocusController.SetFocusTarget();
        }
    }
}
