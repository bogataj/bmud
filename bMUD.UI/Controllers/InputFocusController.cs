﻿using System;
using System.Windows;

namespace bMUD.UI.Controllers
{
    public class InputFocusController : IInputFocusController
    {
        public event InputFocusEventHandler SetFocus;

        public void SetFocusTarget()
        {
            Application.Current.Dispatcher.Invoke(new Action(delegate
            {
                SetFocus?.Invoke();
            }));
        }
    }
}
