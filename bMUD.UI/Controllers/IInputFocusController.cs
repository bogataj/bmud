﻿namespace bMUD.UI.Controllers
{
    public delegate void InputFocusEventHandler();

    public interface IInputFocusController
    {
        void SetFocusTarget();

        event InputFocusEventHandler SetFocus;
    }
}
