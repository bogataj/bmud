﻿using Prism.Interactivity.InteractionRequest;

namespace bMUD.UI.Notifications
{
    public class ConnectNotification : Confirmation
    {
        public string Host { get; set; }
        public string Port { get; set; }
    }
}
