﻿using Microsoft.Practices.Unity;
using Prism.Mvvm;
using System.Windows;

namespace bMUD
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            IUnityContainer _container = new UnityContainer();

            ViewModelLocationProvider.SetDefaultViewModelFactory((type) =>
            {
                return _container.Resolve(type);
            });

            var bootstrapper = new Bootstrapper();
            bootstrapper.Run();
        }
    }
}
