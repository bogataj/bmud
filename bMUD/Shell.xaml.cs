﻿using System;
using MahApps.Metro.Controls;
using Prism.Modularity;
using Prism.Regions;
using System.Windows.Media.Imaging;

namespace bMUD
{
    /// <summary>
    /// Interaction logic for Shell.xaml
    /// </summary>
    public partial class Shell : MetroWindow
    {
        private static Uri WorkspaceViewUri = new Uri("/WorkspaceView", UriKind.Relative);

        private IRegionManager regionManager;
        private IModuleManager moduleManager;

        public Shell(IRegionManager regionManager, IModuleManager moduleManager)
        {
            this.regionManager = regionManager;
            this.moduleManager = moduleManager;

            InitializeComponent();

            this.Icon = new BitmapImage(new Uri("bMUD.ico", UriKind.Relative));

            this.moduleManager.LoadModuleCompleted +=
                (s, e) =>
                {
                    if (e.ModuleInfo.ModuleName == "UIModule")
                    {
                        var navigationParameters = new NavigationParameters();

                        this.regionManager.RequestNavigate(
                            "WorkspaceRegion",
                            WorkspaceViewUri);
                    }
                };
        }
    }
}
